package sort.select;

public class SelectSort {
	public static void main(String[] args) {
		int[] array={10,1,5,9,6,8,7,4,2,3};
		int minIndex,temp;
		for (int i=0;i<array.length-1;i++){
		    minIndex = i;
		    for (int j=i+1;j<array.length;j++){
		        if(array[j]<array[minIndex]){
		            minIndex = j;
		        }
		    }
		    temp = array[i];
		    array[i] = array[minIndex];
		    array[minIndex] = temp;
		}
		for (int num:array){
		    System.out.println(num);
		}
	}
}
