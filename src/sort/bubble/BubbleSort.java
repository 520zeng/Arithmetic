package sort.bubble;

public class BubbleSort {
	public static void main(String[] args) {
		int[] array={10,1,5,9,6,8,7,4,2,3};
		int temp;
		for (int i=0;i<array.length-1;i++){
		    for(int j=0;j<array.length-1-i;j++){
		        if(array[j]>array[j+1]){
		            temp = array[j+1];
		            array[j+1] = array[j];
		            array[j] = temp;
		        }
		    }
		}
		for(int num:array){
		    System.out.println(num);
		}
	}
}
