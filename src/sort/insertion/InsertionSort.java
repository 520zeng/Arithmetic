package sort.insertion;

public class InsertionSort {
	public static void main(String[] args) {
		int[] array={1,3,5,7,9,2,4,6,8,10};
		int preIndex,current;
		for (int i=1;i<array.length;i++){
		    preIndex = i-1;
		    current = array[i];
		    while(preIndex >=0 && array[preIndex]>current){
		        array[preIndex+1]=array[preIndex];
		        preIndex--;
		    }
		    array[preIndex+1] = current;
		}
		for (int num:array){
		    System.out.println(num);
		}
	}
}
